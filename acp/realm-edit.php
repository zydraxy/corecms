<?php
include ('header.php');
include ('sidebar.php');

?>
    <div id="content-wrapper">

    <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="<?php echo $custdir; ?>/acp/">Dashboard</a>
            </li>
        </ol>
        <div class="card mb-3">
            <div class="card-header">
                <i class="fad fa-globe-americas"></i> Rrealm edit</div>
            <div class="card-body">
                <?php
                $get_realm_id = $_GET['id'];
                if(empty($get_realm_id))
                {
                    echo '
                            <div class="alert alert-warning" role="alert">
                                <i class="fad fa-exclamation-triangle"></i> Invalid Realm ID
                            </div>
                            ';
                    header("refresh:3; url=$custdir/acp/realm-settings.php");
                }
                else
                {
                    //papyal thingy
                    if(isset($_POST['realm_update']))
                    {
                        $realm_name = stripslashes(mysqli_real_escape_string($mysqliA, $_POST['realm_name']));
                        $realm_publicIP = stripslashes(mysqli_real_escape_string($mysqliA, $_POST['realm_publicIP']));
                        $realm_localIP = stripslashes(mysqli_real_escape_string($mysqliA, $_POST['realm_localIP']));
                        $realm_port = stripslashes(mysqli_real_escape_string($mysqliA, $_POST['realm_port']));
                        $realm_icon = stripslashes(mysqli_real_escape_string($mysqliA, $_POST['realm_icon']));
                        $realm_flag = stripslashes(mysqli_real_escape_string($mysqliA, $_POST['realm_flag']));
                        $realm_timezone = stripslashes(mysqli_real_escape_string($mysqliA, $_POST['realm_timezone']));
                        $realm_build = stripslashes(mysqli_real_escape_string($mysqliA, $_POST['realm_build']));
                        $realm_region = stripslashes(mysqli_real_escape_string($mysqliA, $_POST['realm_region']));
                        $realm_battleg = stripslashes(mysqli_real_escape_string($mysqliA, $_POST['realm_battleg']));
                        //update
                        $realm_update = $mysqliA->query("UPDATE `realmlist` SET `name` = '$realm_name', `address` = '$realm_publicIP', `localAddress` = '$realm_localIP', `port` = '$realm_port', `icon` = '$realm_icon', `flag` = '$realm_flag', `timezone` = '$realm_timezone', `gamebuild` = '$realm_build', `Region` = '$realm_region', `Battlegroup` = '$realm_battleg' WHERE `id` = '$get_realm_id'; ")  or die (mysqli_error($mysqliA));
                        if($realm_update === true)
                        {
                            echo '
                            <div class="alert alert-success" role="alert">
                                <i class="fad fa-spinner-third fa-spin"></i> Your settings is updating. Please wait...!
                            </div>
                            ';
                            header("refresh:3; url=$custdir/acp/realm-settings.php");
                        }
                        else
                        {
                            echo '
                            <div class="alert alert-warning" role="alert">
                                <i class="fad fa-exclamation-triangle"></i> There\'s been an error! Please try again!<br />If this error continues please contact us on discord!
                            </div>
                                ';
                            header("refresh:5; url=$custidr/acp/realm-settings.php");
                        }
                    }
                    else
                    {
                        //let's get data from db
                        $realme_query = $mysqliA->query("SELECT * FROM `realmlist` WHERE `id` = '$get_realm_id';") or die (mysqli_error($mysqliA));
                        while($res = $realme_query->fetch_assoc())
                        {
                            $realm_id = $res['id'];
                            $realm_name = $res['name'];
                            $realm_publicIP = $res['address'];
                            $realm_localIP = $res['localAddress'];
                            $realm_port = $res['port'];
                            $realm_a_icon = $res['icon'];
                            $realm_a_flag = $res['flag'];
                            $realm_a_timezone = $res['timezone'];
                            $realm_build = $res['gamebuild'];
                            $realm_a_region = $res['Region'];
                            $realm_battleg = $res['Battlegroup'];
                            switch ($realm_a_region){
                                case 1:
                                    $realm_region = 'US';
                                    break;
                                case 2:
                                    $realm_region = 'Europe';
                                    break;
                                case 3:
                                    $realm_region = 'China';
                                    break;
                                case 4:
                                    $realm_region = 'Korea';
                                    break;
                            }
                            switch($realm_a_icon){
                                case 0:
                                    $realm_icon = 'Normal';
                                    break;
                                case 1:
                                    $realm_icon = 'PvP';
                                    break;
                                case 4:
                                    $realm_icon = 'Normal';
                                    break;
                                case 6:
                                    $realm_icon = 'RP';
                                    break;
                                case 8:
                                    $realm_icon = 'RP PvP';
                                    break;
                            }
                            switch ($realm_a_flag){
                                case 0:
                                    $realm_flag = 'None';
                                    break;
                                case 1:
                                    $realm_flag = 'Invalid';
                                    break;
                                case 2:
                                    $realm_flag = 'Offline';
                                    break;
                                case 4:
                                    $realm_flag = 'SpecifyBuild';
                                    break;
                                case 8:
                                    $realm_flag = 'Medium';
                                    break;
                                case 16:
                                    $realm_flag = 'Medium';
                                    break;
                                case 32:
                                    $realm_flag = 'New Players';
                                    break;
                                case 64:
                                    $realm_flag = 'Recommended';
                                    break;
                                case 128:
                                    $realm_flag = 'Full';
                            }
                            switch ($realm_a_timezone){
                                case 1:
                                    $realm_timezone = 'Development';
                                    break;
                                case 2:
                                    $realm_timezone = 'United States';
                                    break;
                                case 3:
                                    $realm_timezone = 'Oceanic';
                                    break;
                                case 4:
                                    $realm_timezone = 'Latin America';
                                    break;
                                case 5:
                                    $realm_timezone = 'Tournament';
                                    break;
                                case 6:
                                    $realm_timezone = 'Korea';
                                    break;
                                case 8:
                                    $realm_timezone = 'English';
                                    break;
                                case 9:
                                    $realm_timezone = 'German';
                                    break;
                                case 10:
                                    $realm_timezone = 'French';
                                    break;
                                case 11:
                                    $realm_timezone = 'Spanish';
                                    break;
                                case 12:
                                    $realm_timezone = 'Russian';
                                    break;
                                case 14:
                                    $realm_timezone = 'Taiwan';
                                    break;
                                case 16:
                                    $realm_timezone = 'China';
                                    break;
                                case 26:
                                    $realm_timezone = 'Test Server';
                                    break;
                                case 30:
                                    $realm_timezone = 'Test Server 2';
                                    break;
                                case 37:
                                    $realm_timezone = 'Russian Tournament';
                                    break;
                                case 49:
                                    $realm_timezone = 'Brazil';
                                    break;
                                case 50:
                                    $realm_timezone = 'Italian';
                                    break;
                                case 51:
                                    $realm_timezone = 'Hyrule';
                                    break;
                                case 55:
                                    $realm_timezone = 'Recommended Realm';
                                    break;
                            }
                            ?>
                            <form name="realm_update" method="post" action="">
                            <div class="form-group">
                                <label for="site_name">Realm Name</label>
                                <input type="text" name="realm_name" class="form-control" value="<?php echo $realm_name; ?>" required>
                                <small>Enter here your realm name</small>
                            </div>
                            <div class="form-group">
                                <label for="site_description">Realm IP Public</label>
                                <input type="text" name="realm_publicIP" class="form-control" value="<?php echo  $realm_publicIP; ?>" required>
                                <small>Enter here your realm public IP address</small>
                            </div>
                            <div class="form-group">
                                <label for="site_contact">Realm IP local</label>
                                <input type="text" name="realm_localIP" class="form-control" value="<?php echo  $realm_localIP; ?>" required>
                                <small>Enter here your realm local IP address</small>
                            </div>
                            <div class="form-group">
                                <label for="site_discord">Realm Port</label>
                                <input type="text" name="realm_port" class="form-control" value="<?php echo  $realm_port; ?>" required>
                                <small>Enter here your realm port</small>
                            </div>
                            <div class="form-group">
                                <label for="realm_icon">Realm Icon</label>
                                <select name="realm_icon" class="form-control">
                                    <option value="<?php echo $realm_a_icon; ?>" selected="selected">Current Icon - <?php echo $realm_icon; ?></option>
                                    <option disabled="disabled">-- Icons --</option>
                                    <option value="0">Normal</option>
                                    <option value="1">PvP</option>
                                    <option value="4">Normal</option>
                                    <option value="6">RP</option>
                                    <option value="8">RP PvP</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="realm_flag">Realm Flag</label>
                                <select name="realm_flag" class="form-control">
                                    <option value="<?php echo $realm_a_flag; ?>" selected="selected">Current Icon - <?php echo $realm_flag; ?></option>
                                    <option disabled="disabled">-- Flag --</option>
                                    <option value="0">None</option>
                                    <option value="1">Invalid</option>
                                    <option value="2">Offline</option>
                                    <option value="4">SpecifyBuild</option>
                                    <option value="8">Medium</option>
                                    <option value="16">Medium</option>
                                    <option value="32">New Players</option>
                                    <option value="64">Recommended</option>
                                    <option value="128">Full</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="realm_timezone">Realm Timezone</label>
                                <select name="realm_timezone" class="form-control">
                                    <option value="<?php echo $realm_a_timezone; ?>" selected="selected">Current timezone - <?php echo $realm_timezone; ?></option>
                                    <option disabled="disabled">-- Timezone --</option>
                                    <option value="1">Development</option>
                                    <option value="2">United States</option>
                                    <option value="3">Oceanic</option>
                                    <option value="4">Latin America</option>
                                    <option value="5">Tournament</option>
                                    <option value="6">Korea</option>
                                    <option value="8">English</option>
                                    <option value="9">German</option>
                                    <option value="10">French</option>
                                    <option value="11">Spanish</option>
                                    <option value="12">Russian</option>
                                    <option value="14">Taiwan</option>
                                    <option value="16">China</option>
                                    <option value="26">Test Server</option>
                                    <option value="30">Test Server 2</option>
                                    <option value="37">Russian Tournament</option>
                                    <option value="49">Brazil</option>
                                    <option value="50">Italian</option>
                                    <option value="51">Hyrule</option>
                                    <option value="55">Recommended Realm</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="realm_build">Realm Build</label>
                                <input type="text" name="realm_build" class="form-control" value="<?php echo  $realm_build; ?>" required>
                                <small>Enter here your realm build number</small>
                            </div>
                            <div class="form-group">
                                <label for="realm_region">Realm Region</label>
                                <select name="realm_region" class="form-control">
                                    <option value="<?php echo $realm_a_region; ?>" selected="selected">Current region - <?php echo $realm_region; ?></option>
                                    <option disabled="disabled">-- Region --</option>
                                    <option value="1">US</option>
                                    <option value="2">Europe</option>
                                    <option value="3">China</option>
                                    <option value="4">Korea</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="realm_battleg">Realm Battlegroup</label>
                                <input type="text" name="realm_battleg" class="form-control" value="<?php echo  $realm_battleg; ?>" required>
                                <small>Enter here your realm battlegroup</small>
                            </div>
                            <button type="submit" name="realm_update" class="btn btn-primary"><i class="fad fa-check-circle"></i> Update this settings!</button>
                        </form>
                        <?php
                        }
                    }
                }
                ?>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
<?php
include ('footer.php');
?>