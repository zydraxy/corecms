function toggleDarkLight() {
    var body = document.getElementById("page-top");
    var currentClass = body.className;
    body.className = currentClass === "dark-mode" ? "light-mode" : "dark-mode";
    setCookie("mode", body.className, 30);
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function start() {
    var mode = getCookie("mode");
    var body = document.getElementById("page-top");

    if (mode !== "") {
        body.className = mode;
    }
}

$(document).ready(function() {
    start();
});